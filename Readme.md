> [Ecommerce-backend](https://gitlab.com/OsouzaTI/ecommerce-backend)
    Projeto backend feito com spring-boot
    
    Para o backend inicializar, antes é necessário estar com o banco de dados postgres
    rodando na porta 5433 e uma instancia do rabbitmq na porta 5672 com a seguinte fila "carrinho_queue"
    pré-criada.

> [prime-ecommerce](https://gitlab.com/OsouzaTI/prime-ecommerce)
    Projeto front-end feito com angular

    Para rodar o front-end antes é necessario rodar o comando "npm i" para
    instalar todas as dependencias do projeto. Apos isso rode o "docker-compose up"
    para subir as instancias do rabbitmq e do postgres (necessários para o passo anterior) e finalmente rode "ng s" para buildar o projeto angular e subir o projeto na porta 4200.